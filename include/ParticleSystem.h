//
//  ParticleSystem.hpp
//  PixelLines
//
//  Created by Joseph Chow on 7/25/17.
//
//

#ifndef ParticleSystem_hpp
#define ParticleSystem_hpp

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/Texture.h"
#include "FeedbackBuffer.hpp"
#include "Mesh.h"
class ParticleSystem {

	int numParticles;
	float pLineLife;

	std::vector<ci::vec3> pos;
	std::vector<ci::vec3> extras;
	std::vector<uint32_t> indices;
	std::vector<ci::vec2> uvs;
	std::vector<ci::vec3> velocities;
	std::vector<ci::vec3> originalPositions;

	FeedbackBufferRef buffer;
	MeshRef particleMesh;

	float time;

	uint32_t mIterationsPerFrame, mIterationIndex;

public:
	ParticleSystem();
	void setup();
	void updateTF();
	void setupTF();
	void update();
	void draw();
};
#endif /* ParticleSystem_hpp */
