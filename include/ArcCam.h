#pragma once
#include "cinder/Camera.h"
#include "cinder/CameraUi.h"
#include "cinder/Timeline.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
typedef std::shared_ptr<class ArcCam> ArcCamRef;

class ArcCam {
	ci::CameraPersp mCam;
	ci::CameraUi mCamUi;
	ci::Anim<ci::vec2> mMouse;
	ci::vec3 target;
public:
	ArcCam(float fov, float aspect, float near, float far, ci::vec3 eye, ci::vec3 target);
	static ArcCamRef create(float fov = 60.0,
		float aspect = ci::app::getWindowAspectRatio(),
		float near = 0.1,
		float far = 10000.0,
		ci::vec3 eye = ci::vec3(0, 0, 50),
		ci::vec3 target = ci::vec3(0, 0, 0)) {
		return ArcCamRef(new ArcCam(fov, aspect, near, far, eye, target));
	}

	//! Returns the eye point of the camera
	ci::vec3 getEye() {
		return mCam.getEyePoint();
	}

	//! moves the camera around the currently set target.
	//! Takes the speed as a parameter which should be a constant value.
	void orbitTarget(float speed);

	//! Same as above but accepts an alternate target
	void orbitTarget(float speed, ci::vec3 target);

	//! On desktop - enable "tweening" of mouse drag camera rotation
	void enableTweening();

	//! sets camera target
	void setTarget(ci::vec3 target);

	//! sets camera far value
	void setFar(float far);

	//! sets camera near value
	void setNear(float near);

	//! Sets the camera zoom
	void setZoom(float zoom);

	//! rotates the view matrix
	void rotateView(ci::mat4 rotateMat);

	//! use the matrix based on the perspective camera.
	//! Use with 3d content
	void useMatrices();

	//! use orthographic projection instead based on the current window
	//! use with 2d content
	void orthoMatrices();

	ci::quat getOrientation() {
		return mCam.getOrientation();
	}

	ci::mat4 getProjection() {
		return mCam.getProjectionMatrix();
	}

	ci::mat4 getView() {
		return mCam.getViewMatrix();
	}

	ci::vec3 getViewDirection() {
		return mCam.getViewDirection();
	}
};
