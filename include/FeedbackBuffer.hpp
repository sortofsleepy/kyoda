//
//  FeedbackBuffer.hpp
//  VolParticles
//
//  Created by Joseph Chow on 7/14/17.
//
//

#ifndef FeedbackBuffer_hpp
#define FeedbackBuffer_hpp

#include "cinder/gl/Vao.h"
#include "cinder/gl/Vbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/BufferTexture.h"
#include <vector>
#include <string>
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
#include "cinder/gl/gl.h"


using namespace std;
/**
 
 A simple TransformFeedback class for using transform feedback to manipulate data.
 
 Relies on using seperate attributes vs interleaved at the moment.
 
 */

typedef std::shared_ptr<class FeedbackBuffer> FeedbackBufferRef;

typedef struct {
	ci::gl::VboRef vbos[2];
}BufferAttribute;


class FeedbackBuffer {

	// variables to help with swapping
	uint32_t mIterationsPerFrame, mIterationIndex;

	// the mode to do feedback on
	GLenum mode;

	ci::gl::GlslProgRef updateShader;
	
	float time;

	int flag;

public:
	// number of points to do feedback on
	int numPoints;
	std::vector<BufferAttribute> attribs;
	ci::gl::VaoRef vaos[2];


	static FeedbackBufferRef create() {
		return FeedbackBufferRef(new FeedbackBuffer());
	}

	FeedbackBuffer();
	~FeedbackBuffer();

	

	void setup();
	void loadShader(std::string path, std::vector<std::string> feedbackVaryings);

	//! updates the transform feedback process. Pass in an optional callback to update the shader uniforms.
	void update(std::function<void(ci::gl::GlslProgRef)> uniformUpdateFunc=nullptr);

	void vertexAttribPointer(int index,int size,GLenum data_type=GL_FLOAT,GLboolean normalized=GL_FALSE,int stride=0,
		const GLvoid * pointer= (const GLvoid*)0);

	template<typename T> 
	void bufferAttribute(std::vector<T> data) {
		BufferAttribute attrib;
		attrib.vbos[0] = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_STATIC_DRAW);
		attrib.vbos[1] = ci::gl::Vbo::create(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_STATIC_DRAW);

		attribs.push_back(attrib);
	}

	BufferAttribute getAttribute(int index = 0) {
		return attribs[index];
	}

	// returns one of the Vbos from a BufferAttribute for the purposes of re-using
	// the calculated data elsewhere.
	ci::gl::VboRef getAttributeBuffer(int index = 0) {
		return attribs[index].vbos[1];
	}


	

	void updateTime(float time) {
		this->time = time;
	}

	ci::gl::VaoRef getVao() {
		return vaos[flag];
	}
};

#endif