#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"
#include <vector>
#include <memory>


typedef std::shared_ptr<class Mesh> MeshRef;

typedef struct{
	ci::geom::Attrib attrib;
	size_t dataSizeBytes;
	const void * data;
}MeshAttrib;

class Mesh {

	ci::gl::VboMeshRef mMesh;
	ci::gl::BatchRef mBatch;
	ci::gl::GlslProgRef mShader;

	std::map < ci::geom::Attrib, std::string > customAttribMapping;

	// stores VBO based buffer data along with the layout description.
	std::vector<std::pair<ci::geom::BufferLayout, ci::gl::VboRef> > layoutData;

	// meant to store regular mesh data via vectors
	std::vector<MeshAttrib> meshAttribs;

	// the layout object used for building a regular mesh.
	ci::gl::VboMesh::Layout meshLayout;

	// vector of indices
	std::vector<uint32_t> indices;
	
	// the current number of vertices that needs to be drawn.
	int vertexCount;

	// the primitive type in which to use to draw the final mesh.
	GLenum primitiveType;

public:
	Mesh(int vertexCount, GLenum primitiveType);

	static MeshRef create(int vertexCount = 1,GLenum primitiveType=GL_TRIANGLES) {
		return MeshRef(new Mesh(vertexCount,primitiveType));
	}
	void addIndex(int index) {
		indices.push_back(index);
	}

	void addIndices(std::vector<uint32_t> indices) {
		this->indices = indices;
	}

	void draw() {
		mBatch->draw();
	}

	void setVertexCount(int vertexCount) {
		this->vertexCount = vertexCount;
	}

	void setPrimitiveType(GLenum primitiveType) {
		this->primitiveType = primitiveType;
	}

	void loadShader(std::string vertex, std::string fragment, std::string geom = "");

	// Adds a new attribute to the mesh. Pass in VboRef containing attribute data
	void bufferAttrib(ci::geom::Attrib attrib, ci::gl::VboRef vbo, int size = 3, int stride = 0, int offset = 0);

	// Adds a new attribute to the mesh without using vbos as data storage, but  instead allows
	// the mesh to build up it's own vbos.
	void bufferAttrib(ci::geom::Attrib,size_t dataSizeBytes, const void * data);

	// builds the mesh 
	void compileMesh(ci::gl::VboMesh::Layout layout);
	void compileMesh(std::map < ci::geom::Attrib, std::string > attribMap);
	void compileMesh(std::map < ci::geom::Attrib, std::string > attribMap, ci::gl::VboMesh::Layout layout);


	void compileMesh();

};