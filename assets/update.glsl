
#version 330 core


layout (location = 0) in vec3 position;
layout (location = 1) in vec3 velocity;
layout (location = 2) in vec3 extra;
layout (location = 3) in vec3 originalPosition;

uniform float ciElapsedSeconds;
uniform float time;
uniform float uSphereSize;
uniform float uEnd = 100.0;
uniform float uNumSeg = 40.0;
uniform float uLength = 30.0;
uniform vec3 uHit;

// The outputs of the vertex shader are the same as the inputs
out vec3 oPosition;
out vec3 oVelocity;
out vec3 oExtra;
out vec3 oOriginalPosition;

#include "utils.glsl"

void main(void)
{
    vec3 pos = position;  // p can be our position
    vec3 vel = velocity;
    vec3 orgPos = originalPosition;
    vec3 ext = extra;
    
    
    float posOffset = mix(ext.r, 1.0, .925) * 0.02;
    //vec3 acc = curlNoise(pos * posOffset + ciElapsedSeconds * .35);
    vec3 acc = curlNoise(pos * posOffset + time * .0035);
    
    
    
    float speed = 1.0 + ext.g * 0.5;
    speed = pow(speed, 2.0) * (1.5 + acc.r);
    
    if(ext.b < uNumSeg) {
        speed *= 0.001;
    }
    
    vel += acc * .001 * speed;
    vec3 dir = normalize(pos);
    vel += dir * 0.002 * speed;

    
    const float decrease = .93;
    vel *= decrease;
    
    ext.b += 1.0;
    pos += vel;
    
    if(ext.b > uEnd + uLength) {
        vec3 axis = normalize(extra);
        orgPos = rotate(orgPos, axis, time * ext.r * ext.g);
        pos = orgPos;
        ext.b = 0.0;
    }
    // Write the outputs
    oPosition = pos;
    oVelocity = vel;
    oExtra = ext;
    oOriginalPosition = orgPos;
}
