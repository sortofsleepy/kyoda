#version 330 core

//layout (location = 0) in vec3 position;	// POSITION_INDEX

in vec3 ciPosition;

uniform mat4 ciModelViewProjection;

out float life;

void main(void)
{
	gl_Position = ciModelViewProjection * vec4(ciPosition, 1.0);

}
