//
//  ParticleSystem.cpp
//  PixelLines
//
//  Created by Joseph Chow on 7/25/17.
//
//

#include "ParticleSystem.h"
#include "cinder/app/App.h"
#include "cinder/Rand.h"
#include "cinder/Log.h"
using namespace std;
using namespace ci;

ParticleSystem::ParticleSystem() :numParticles(100),
pLineLife(100.0),
time(0.0f),
mIterationIndex(0),
mIterationsPerFrame(2){}


void ParticleSystem::draw() {
	
	
	//gl::ScopedVao v(vaos[mIterationIndex & 1]);
	gl::ScopedVao v(buffer->vaos[mIterationIndex & 1]);

	gl::setDefaultShaderVars();

	gl::pointSize(1.0f);
	//gl::drawArrays(GL_POINTS, 0, numParticles * numParticles);
	particleMesh->draw();

	
}

void ParticleSystem::update() {
	time += 1.0;
	updateTF();
	
}


void ParticleSystem::updateTF() {

	buffer->update([=](gl::GlslProgRef shader)->void {
		shader->uniform("time", time);
	});
}
void ParticleSystem::setupTF() {

	buffer = FeedbackBuffer::create();
	buffer->setup();
	

	std::vector<string> varyings = {
		"oPosition",
		"oVelocity",
		"oExtra",
		"oOriginalPosition"
	};

	buffer->loadShader("update.glsl", varyings);
	buffer->numPoints = pos.size();


	// load attributes we want to animate into the Transform Feedback buffer.
	buffer->bufferAttribute(pos);
	buffer->vertexAttribPointer(0, 3);

	buffer->bufferAttribute(velocities);
	buffer->vertexAttribPointer(1, 3);

	buffer->bufferAttribute(extras);
	buffer->vertexAttribPointer(2, 3);

	buffer->bufferAttribute(originalPositions);
	buffer->vertexAttribPointer(3, 3);
	
}

void ParticleSystem::setup() {


	float range = 4.0f;
	int size = numParticles * numParticles;


	for (int j = 0; j < numParticles; ++j) {
		for (int i = 0; i < numParticles; ++i) {
			auto randX = randFloat(-range, range);
			auto randY = randFloat(-range, range);
			auto randZ = randFloat(-range, range);

			pos.push_back(vec3(randX, randY, randZ));
			extras.push_back(vec3(randFloat(), randFloat(), randFloat(1.0f, pLineLife)));
		}
	}

	// build original positions
	for (int i = 0; i < pos.size(); ++i) {
		originalPositions.push_back(pos.at(i));
	}

	// build initial velocities
	for (int i = 0; i < pos.size(); ++i) {
		velocities.push_back(vec3(1.0));
	}

	setupTF();

	particleMesh = Mesh::create();
	particleMesh->setVertexCount(pos.size());
	particleMesh->setPrimitiveType(GL_POINTS);
	particleMesh->loadShader("render.vert", "render.frag");
	particleMesh->bufferAttrib(geom::POSITION, buffer->getAttribute(0).vbos[1]);
	particleMesh->compileMesh();


}
