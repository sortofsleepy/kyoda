//
//  FeedbackBuffer.cpp
//  VolParticles
//
//  Created by Joseph Chow on 7/14/17.
//
//

#include "FeedbackBuffer.hpp"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

FeedbackBuffer::FeedbackBuffer():flag(0),
numPoints(100),
mIterationsPerFrame(1),
mIterationIndex(0),
time(0.0){}

FeedbackBuffer::~FeedbackBuffer() {
	
}

void FeedbackBuffer::setup() {
	for (int i = 0; i < 2; ++i) {
		vaos[i] = gl::Vao::create();
	}
}
void FeedbackBuffer::vertexAttribPointer(int index, int size, GLenum data_type,GLboolean normalized, int stride,const GLvoid * pointer) {
	{
		for (int i = 0; i < 2; ++i) {
			ci::gl::ScopedVao scopeVao(vaos[i]);
			{
				// bind and explain the vbo to your vao so that it knows how to distribute vertices to your shaders.
				ci::gl::ScopedBuffer sccopeBuffer(attribs[index].vbos[i]);
				ci::gl::vertexAttribPointer(index, size, data_type, normalized, stride, pointer);
				ci::gl::enableVertexAttribArray(index);
			}
		}
	}
}
void FeedbackBuffer::loadShader(std::string path, std::vector<std::string> feedbackVaryings) {

	gl::GlslProg::Format updateFormat;
	updateFormat.vertex(app::loadAsset(path))
		// Because we have separate buffers with which
		// to capture attributes, we're using GL_SEPERATE_ATTRIBS
		.feedbackFormat(GL_SEPARATE_ATTRIBS)
		// We also send the names of the attributes to capture
		.feedbackVaryings(feedbackVaryings);

	updateShader = gl::GlslProg::create(updateFormat);

}

void FeedbackBuffer::update(std::function<void(ci::gl::GlslProgRef)> uniformUpdateFunc) {

	// bind shader
	gl::ScopedGlslProg	scopeGlsl(updateShader);
	
	if (uniformUpdateFunc != nullptr) {
		uniformUpdateFunc(updateShader);
	}
	

	// discard fragment state
	gl::ScopedState		scopeState(GL_RASTERIZER_DISCARD, true);


	for (auto i = mIterationsPerFrame; i != 0; --i) {
		// Bind the vao that has the original vbo attached,
		// these buffers will be used to read from.
		gl::ScopedVao scopedVao(vaos[mIterationIndex & 1]);
		// Bind the BufferTexture, which contains the positions
		// of the first vbo. We'll cycle through the neighbors
		// using the connection buffer so that we can derive our
		// next position and velocity to write to Transform Feedback

		// We iterate our index so that we'll be using the
		// opposing buffers to capture the data
		mIterationIndex++;


		// Now bind our opposing buffers to the correct index
		// so that we can capture the values coming from the shader
		//gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, attribs[0].vbos[mIterationIndex & 1]);

		for (int a = 0; a < attribs.size(); ++a) {
			gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, a, attribs[a].vbos[mIterationIndex & 1]);
		}

		// Begin Transform feedback with the correct primitive,
		// In this case, we want GL_POINTS, because each vertex
		// exists by itself
		gl::beginTransformFeedback(GL_POINTS);
		// Now we issue our draw command which puts all of the
		// setup in motion and processes all the vertices
		gl::drawArrays(GL_POINTS, 0, numPoints);
		// After that we issue an endTransformFeedback command
		// to tell OpenGL that we're finished capturing vertices
		gl::endTransformFeedback();
	}

}