#include "Mesh.h"
#include "cinder/app/App.h"
#include "cinder/Log.h"
using namespace std;
using namespace ci;


Mesh::Mesh(int vertexCount, GLenum primitiveType) :vertexCount(vertexCount),
primitiveType(primitiveType) {
}

void Mesh::bufferAttrib(ci::geom::Attrib attrib, ci::gl::VboRef vbo, int size, int stride, int offset) {

	ci::geom::BufferLayout layout;
	layout.append(attrib, size, stride, offset);

	std::pair<ci::geom::BufferLayout, ci::gl::VboRef> attribData = {
		layout,
		vbo
	};


	layoutData.push_back(attribData);

}

void Mesh::bufferAttrib(geom::Attrib attrib, size_t dataSizeBytes, const void  * data) {
	MeshAttrib meshAttribute;

	meshAttribute.attrib = attrib;
	meshAttribute.data = data;
	meshAttribute.dataSizeBytes = dataSizeBytes;

	meshAttribs.push_back(meshAttribute);
}

void Mesh::compileMesh() {
	


	// if we just have regular data
	if (layoutData.size() < 1) {
		// if we don't have indices
		if (indices.size() < 1) {
			mMesh = gl::VboMesh::create(vertexCount, primitiveType, { meshLayout });
		}
		else {
			mMesh = gl::VboMesh::create(vertexCount, primitiveType, { meshLayout },indices.size(),GL_UNSIGNED_SHORT);
			mMesh->bufferIndices(sizeof(uint32_t) *  indices.size(), indices.data());
		}

		for (int i = 0; i < meshAttribs.size(); ++i) {
			mMesh->bufferAttrib(meshAttribs[i].attrib, meshAttribs[i].dataSizeBytes, meshAttribs[i].data);
		}
	}

	if (layoutData.size() > 0) {

		// if we also have regular mesh attributes
		if (meshAttribs.size() > 0) {
			// if we don't have indices
			if (indices.size() < 1) {
				mMesh = gl::VboMesh::create(vertexCount, primitiveType, { meshLayout });
			}
			else {
				mMesh = gl::VboMesh::create(vertexCount, primitiveType, { meshLayout }, indices.size(), GL_UNSIGNED_SHORT);
				mMesh->bufferIndices(sizeof(uint32_t) *  indices.size(), indices.data());
			}

			for (int i = 0; i < meshAttribs.size(); ++i) {
				mMesh->bufferAttrib(meshAttribs[i].attrib, meshAttribs[i].dataSizeBytes, meshAttribs[i].data);
			}

			for (int i = 0; i < meshAttribs.size(); ++i) {
				mMesh->bufferAttrib(meshAttribs[i].attrib, meshAttribs[i].dataSizeBytes, meshAttribs[i].data);
			}

			// append all the VBOs that were added outside of the regular process
			for (int i = 0; i < layoutData.size(); ++i) {
				mMesh->appendVbo(get<0>(layoutData[i]), get<1>(layoutData[i]));
			}
		}
		else {
			if (indices.size() < 1) {
				mMesh = gl::VboMesh::create(vertexCount, primitiveType,layoutData);
			}
			else {
				mMesh = gl::VboMesh::create(vertexCount, primitiveType, layoutData, indices.size(), GL_UNSIGNED_SHORT);
				mMesh->bufferIndices(sizeof(uint32_t) *  indices.size(), indices.data());
			}
		}


	}
	
	if (!mShader) {
		CI_LOG_E("Mesh::compileMesh - shader has not been built");
	}
	else {
		// build batch
		if (customAttribMapping.size() > 0) {
			mBatch = gl::Batch::create(mMesh, mShader,customAttribMapping);
		}
		else {
			mBatch = gl::Batch::create(mMesh, mShader);
		}
	}
}
void Mesh::compileMesh(gl::VboMesh::Layout layout) {
	this->meshLayout = layout;
	compileMesh();
}

void Mesh::compileMesh(std::map < ci::geom::Attrib, std::string > attribMap) {
	customAttribMapping = attribMap;
	compileMesh();
}

void Mesh::compileMesh(std::map < ci::geom::Attrib, std::string > attribMap, gl::VboMesh::Layout layout) {
	this->meshLayout = layout;
	this->customAttribMapping = attribMap;
	compileMesh();
}

void Mesh::loadShader(std::string vertex, std::string fragment, std::string geom) {
	gl::GlslProg::Format fmt;

	fmt.vertex(app::loadAsset(vertex));
	fmt.fragment(app::loadAsset(fragment));
	if (geom != "") {
		fmt.geometry(app::loadAsset(geom));
	}

	mShader = gl::GlslProg::create(fmt);
}
