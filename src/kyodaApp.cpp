#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "ParticleSystem.h"
#include "ArcCam.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class kyodaApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;

	ArcCamRef mCam;
	ParticleSystem system;
};

void kyodaApp::setup()
{
	mCam = ArcCam::create();
	mCam->setZoom(-50);
	system.setup();
}

void kyodaApp::mouseDown( MouseEvent event )
{
}

void kyodaApp::update()
{
	system.update();
}

void kyodaApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
	mCam->useMatrices();
	system.draw();
}

CINDER_APP( kyodaApp, RendererGl )
